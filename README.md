# UŽDUOTIS #


Panaudojus išmoktus daugiamatės statistinės analizės ir duomenų tyrybos metodus bei
programines priemones, atlikti pateiktų duomenų statistinę analizę: parinkti tinkamus statistinės
analizės metodus, sudaryti duomenų analizės modelius, sukurti atitinkamas programas, atlikti
pateiktų duomenų analizę ir parengti statistinės analizės rezultatų ataskaitą su išvadomis. 